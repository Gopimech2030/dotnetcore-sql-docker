using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DockerApi.Entities;

namespace DockerApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HumourController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<HumourController> _logger;
        private readonly GenericContext _context;

        public HumourController(ILogger<HumourController> logger, GenericContext context)
        {
            _logger = logger;
            _context = context;
        }

        [HttpGet]
        [Route("{name}")]
        public string Get(string name)
        {
            return $"Hello {name}!";
        }

        [HttpGet]
        public IEnumerable<User> GetUsers()
        {
            return _context.Users;
        }
    }
}
