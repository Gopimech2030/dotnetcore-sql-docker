using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using DockerApi.Entities;
using static System.Console;
using System.Linq;

namespace DockerApi.Initialisers
{
    public class InitialiseDatabase
    {
        public static void Initialise(IApplicationBuilder app)
        {
            using (var service = app.ApplicationServices.CreateScope())
            {
                SeedData(service.ServiceProvider.GetService<GenericContext>());
            }
        }

        private static void SeedData(GenericContext context)
        {
            WriteLine("Applying Migrations...");
            context.Database.Migrate();

            if (!context.Users.Any())
            {
                WriteLine("Adding data...");
                context.Users.AddRange(
                    new User {FirstName = "Gopikiran", LastName = "Lakkoju", Age = 33},
                    new User {FirstName = "Raghu", LastName = "Tatikonda", Age = 29},
                    new User {FirstName = "Balaji", LastName = "Ummadisetti", Age = 27},
                    new User {FirstName = "Chandrika", LastName = "Chandrika", Age = 27},
                    new User {FirstName = "Sandeep", LastName = "Puchakaayalu", Age = 35},
                    new User {FirstName = "Dwijesh", LastName = "Jamwal", Age = 30}
                );

                context.SaveChanges();
            }
            else
            {
                WriteLine("Data already exist...");
            }
        }
    }
}