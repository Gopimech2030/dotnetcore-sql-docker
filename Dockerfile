# get base .net core sdk from Microsoft
FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /app

# copy cs.proj file and restore any dependencies via - nuget
COPY *.csproj ./
RUN dotnet restore

# copy the project files and build release
COPY . .
RUN dotnet publish -c Release -o output

# generate runtime image
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
WORKDIR /app
EXPOSE 80
COPY --from=build /app/output .
ENTRYPOINT [ "dotnet", "DockerApi.dll" ]