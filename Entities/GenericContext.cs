using Microsoft.EntityFrameworkCore;
using DockerApi.Entities;

namespace DockerApi.Entities
{
    public class GenericContext : DbContext
    {
        public GenericContext(DbContextOptions<GenericContext> options) : base(options)
        {

        }

        public DbSet<User> Users { get; set; }
    }
}